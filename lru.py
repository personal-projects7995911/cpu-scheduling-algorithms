from data import *

def LRU(pages, frame_size):

    page_faults = 0     # zmienna przechowująca ilość podmian stron
    frames = []         # lista zawierająca ramki


    print('Krok \t    Potrzebne  \t  Brakujace \t Ramki \t\t')

    for i,page in enumerate(pages):

        # jeśli ramki nie są wypełnione
        if len(frames) < frame_size:
            # sprawdza czy strona jest już w ramkach, jeśli nie zwiększa ilość błędów o 1 i dodaje stronę
            if page.value not in [f.value for f in frames]:
                page_faults += 1
                page.wait_time = i
                frames.append(page)
            # jeśli strona jest w ramkach nadpisuje czas ostatniego użycia strony
            else:
                for p in frames:
                    if p.value == page.value: p.wait_time = i


        else:
            # sprawdza czy strona jest już w ramkach, jeśli nie zwiększa ilość błędów o 1 i dodaje nową stronę w miejsce tej która była używana najdawniej
            if page.value not in [f.value for f in frames]:
                page_faults += 1
                # znajduje indeks strony z najdłuższym czasem w ramce
                for j,frame in enumerate(frames):
                    if int(frame.wait_time) == int(min([f.wait_time for f in frames])):
                        index = int(j)
                        #usuwa stronę i wstawia nową
                        page.wait_time = i
                        frames.pop(index)
                        frames.insert(index,page)
                        break
            else:
                # jeśli strona jest w ramkach nadpisuje czas ostatniego użycia strony
                for p in frames:
                    if p.value == page.value: p.wait_time = i

        
        # wyświetla aktualny stan pamięci
        try: 
            if pages[i+1].value in [f.value for f in frames]: 
                print(f'{i}  \t|\t {pages[i+1].value} \t|\t X \t|\t {[f.value for f in frames]}')
            else: 
                print(f'{i} \t|\t {pages[i+1].value} \t|\t {pages[i+1].value} \t|\t {[f.value for f in frames]}')

        except:
            print(f'{i} \t|\t X \t|\t X \t|\t {[f.value for f in frames]}')
            print(f'Ilość błędów: {page_faults}')



if __name__ == "__main__":
    frame_size = 4 # ilość ramek
    LRU(frames_data_1, frame_size)
    LRU(frames_data_2, frame_size)
    LRU(frames_data_3, frame_size)
    LRU(frames_data_4, frame_size)
    LRU(frames_data_5, frame_size)
    LRU(frames_data_6, frame_size)
    LRU(frames_data_7, frame_size)
    LRU(frames_data_8, frame_size)