from data import *


def SJFScheduler(processes):
    current_time = 0            # zmienna przechowująca aktualny czas systemu
    ready_queue = []            # lista procesów gotowych do przetworzenia
    average_wait_time = []      # lista przechowująca czas oczekiwania każdego procesu po zakończeniu

    while len(processes) > 0 or len(ready_queue) > 0:

        # tworzy listę procesów gotowych, które przybyły do systemu w aktualnym czasie i dodaje do kolejki
        ready_processes = [p for p in processes if p.arrive_time <= current_time]
        ready_processes.sort(key=lambda x: x.exec_time)
        ready_queue += ready_processes
        # usuwa z listy procesów procesy które już dotarły
        for p in ready_processes:
            processes.remove(p)
    


        if len(ready_queue) > 0:

            if not ready_queue[0].is_processed:
                ready_queue[0].is_processed = True

            # zmniejsza czas wykonywania procesu o 1
            ready_queue[0].exec_time -= 1

            # jeśli czas wykonania procesu wynosi 0 wyświetla informacjie o jego zakończeniu
            if ready_queue[0].exec_time == 0:
                print(f"Proces {ready_queue[0].name} wykonano, czas oczekiwania: {ready_queue[0].wait_time}, czas zakonczenia: {current_time+1}")
                average_wait_time.append(ready_queue[0].wait_time)
                ready_queue.pop(0)
                # sortuje procesy gotowe według czasu wykonania
                if len(ready_queue) != 0: ready_queue.sort(key=lambda x: x.exec_time) 



        # dla każdego procesu w kolejce który nie jest aktualnie przetwarzany zwiększ czas oczekiwania o 1
        for process in ready_queue:
            if process.is_processed == False:
                process.wait_time += 1

        current_time += 1
    # wyświetl średni czas oczekiwania dla wszystkich procesów
    print('Średni czas oczekiwania: ' + str(sum(average_wait_time)/len(average_wait_time)))
    return str(sum(average_wait_time)/len(average_wait_time))

if __name__ == "__main__":
    SJFScheduler(processes_data_1)
    SJFScheduler(processes_data_2)
    SJFScheduler(processes_data_3)
