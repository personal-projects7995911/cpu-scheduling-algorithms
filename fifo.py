from data import *

def FIFO(pages, frame_size):

    page_faults = 0     # zmienna przechowująca ilość podmian stron
    frames = []         # lista zawierająca ramki


    print('Krok  \t  Potrzebne \t Brakujace \t\t  Ramki')
    for i,page in enumerate(pages):
        
        # jeśli ramki nie są wypełnione
        if len(frames) < frame_size:
            # sprawdza czy strona jest już w ramkach, jeśli nie zwiększa ilość błędów o 1 i dodaje stronę
            if page.value not in [p.value for p in frames]:
                page_faults += 1
                frames.append(page)

        else:
            # sprawdza czy strona jest już w ramkach, jeśli nie wyrzuca pierwszą dodaną i dodaje nową stronę na koniec listy
            if page.value not in [p.value for p in frames]:
                page_faults += 1
                frames.pop(0)
                frames.append(page)
                
        # wyświetla aktualny stan pamięci
        try: 
            if pages[i+1].value in [f.value for f in frames]: 
                brakujace = 'X'
                print(f'{i} {page_faults} \t|\t {pages[i+1].value}  \t|\t {brakujace}  \t|\t {[f.value for f in frames]}')
            else: 
                brakujace = pages[i+1]
                print(f'{i} {page_faults} \t|\t {pages[i+1].value}  \t|\t {brakujace.value} \t|\t {[f.value for f in frames]}')
        except:
            print(f'{i} \t|\t X \t|\t X \t|\t {[f.value for f in frames]}')
            print(f'Ilość błędów: {page_faults}')



if __name__ == "__main__":
    frame_size = 4 # ilość ramek
    FIFO(frames_data_1, frame_size)
    FIFO(frames_data_2, frame_size)
    FIFO(frames_data_3, frame_size)
    FIFO(frames_data_4, frame_size)
    FIFO(frames_data_5, frame_size)
    FIFO(frames_data_6, frame_size)
    FIFO(frames_data_7, frame_size)
    FIFO(frames_data_8, frame_size)
    