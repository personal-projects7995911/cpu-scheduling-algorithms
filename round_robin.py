from data import *

def RoundRobinScheduler(processes, quantum_duration):
    current_time = 0                        # zmienna przechowująca aktualny czas systemu
    ready_queue = []                        # lista procesów gotowych do przetworzenia
    remaining_quantum = quantum_duration    # zmienna przechowująca aktualny stan kwantu
    average_wait_time = []                  # lista przechowująca czas oczekiwania każdego procesu po zakończeniu
    temp = []
    while len(processes) > 0 or len(ready_queue) > 0:
    
        # tworzy listę procesów gotowych, które przybyły do systemu w aktualnym czasie i dodaje do kolejki
        ready_processes = [p for p in processes if p.arrive_time <= current_time]
        ready_queue += ready_processes
        if len(temp) != 0: ready_queue += temp
        temp = []

        # usuwa z listy procesów procesy które już dotarły
        for p in ready_processes:
            processes.remove(p)

        if len(ready_queue) > 0:
            
            if not ready_queue[0].is_processed:
                ready_queue[0].is_processed = True

            # zmniejsza czas wykonywania procesu oraz aktualnego kwantu o 1
            ready_queue[0].exec_time -= 1
            remaining_quantum -= 1

            if ready_queue[0].exec_time <= 0 or remaining_quantum == 0 and len(ready_queue) != 1:

                # jeśli proces nie zakończył się, przesuwa go na koniec kolejki
                if ready_queue[0].exec_time > 0:
                    ready_queue[0].is_processed = False
                    current_process = ready_queue.pop(0)
                    temp.append(current_process)
                    remaining_quantum = quantum_duration
                else:
                    # jeśli proces zakończył się wyświetla informacjie o jego zakończeniu i usuwa go z kolejki
                    print(f"Proces {ready_queue[0].name} wykonano, czas oczekiwania: {ready_queue[0].wait_time}, czas zakonczenia: {current_time}")
                    average_wait_time.append(ready_queue[0].wait_time)
                    ready_queue.pop(0)
                    
                    remaining_quantum = quantum_duration

        
        # dla każdego procesu w kolejce który nie jest aktualnie przetwarzany zwiększa czas oczekiwania o 1
        for process in ready_queue:
            if not process.is_processed:
                process.wait_time += 1

        current_time += 1

        # wyświetla średni czas oczekiwania dla wszystkich procesów
        if len(ready_queue) == 0 and len(processes) == 0:
            print('Średni czas oczekiwania: ' +
                str(sum(average_wait_time)/len(average_wait_time)))


if __name__ == "__main__":
    quantum = 3
    RoundRobinScheduler(processes_data_1, quantum)
    RoundRobinScheduler(processes_data_2, quantum)
    RoundRobinScheduler(processes_data_3, quantum)
    