
class Process:
  def __init__(self, name, arrive_time, exec_time):
    self.name = name
    self.arrive_time = arrive_time
    self.exec_time = exec_time
    self.wait_time = 0
    self.is_processed = False
